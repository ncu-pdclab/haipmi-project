#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess
import logging
import time
import ConfigParser
import re

class IPMIManager(object):  
    def __init__(self):
        self.config = ConfigParser.RawConfigParser()
        self.config.read('hass.conf')
        self.ipmi_IP_dict = dict(self.config._sections['ipmi'])

    def rebootNode(self, nodeID):
	code = ""
	message = ""
        try:
	    command = "ipmitool -I lanplus -H %s -U root -P pdclab chassis power reset" % self.ipmi_IP_dict[nodeID]
            execute_message = subprocess.check_output(command, shell = True)
            if "Reset" in execute_message:
                message = "The Computing Node %s is rebooted." % nodeID
                logging.info("IpmiModule rebootNode - The Computing Node %s is rebooted." % nodeID)
                code = "0"
        except Exception as e:
            message = "The Computing Node %s can not be rebooted." % nodeID
            logging.error("IpmiModule rebootNode - %s" % e)
            code = "1"
        finally:
            result = {"code":code, "node":nodeID, "message":message}
            return result

    def startNode(self, nodeID):
	code = ""
	message = ""
	try:
	    command = "ipmitool -I lanplus -H %s -U root -P pdclab chassis power on" % self.ipmi_IP_dict[nodeID]
	    execute_message = subprocess.check_output(command, shell = True)
	    if "Up/On" in execute_message:
		message = "The Computing Node %s is started." % nodeID
	    	logging.info("IpmiModule startNode - The Computing Node %s is started." % nodeID)
		code = "0"
	except Exception as e:
	    message = "The Computing Node %s can not be started." % nodeID
	    logging.error("IpmiModule startNode - %s" % e)
	    code = "1"
	finally:
	    result = {"code":code, "node":nodeID, "message":message}
	    return result
	
    def shutOffNode(self, nodeID):
	code = ""
        message = ""
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab chassis power off" % self.ipmi_IP_dict[nodeID]
            execute_message = subprocess.check_output(command, shell = True)
            if "Down/Off" in execute_message:
                message = "The Computing Node %s is shut down." % nodeID
                logging.info("IpmiModule shutOffNode - The Computing Node %s is shut down." % nodeID)
                code = "0"
        except Exception as e:
            message = "The Computing Node %s can not be shut down." % nodeID
            logging.error("IpmiModule shutOffNode - %s" % e)
            code = "1"
        finally:
            result = {"code":code, "node":nodeID, "message":message}
            return result

    def getAllInfoOfNode(self, nodeID):
        code = ""
        message = ""
        data_list = []
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab sdr elist full -v -c sensor reading" % self.ipmi_IP_dict[nodeID]
	    p = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=True)
            sensor_output, err = p.communicate()
            sensor_output = sensor_output.split("\n")[5:-1]
            data_list = self._dataClean(sensor_output)
            code = "0"
            message = "Successfully get computing node : %s's hardware information." % nodeID
            logging.info("IpmiModule getNodeInfo - " + message)
        except Exception as e:
            code = "1"
            message = "Error! Unable to get computing node : %s's hardware information." % nodeID
            logging.error("IpmiModule getNodeInfo - " + err)
        finally:
            result = {"code" : code, "info" : data_list, "message" : message}
            return result

    def _dataClean(self, raw_data_list):
        result = []
        for raw_data in raw_data_list:
            raw_data = raw_data.split(",")
            value = raw_data[1] + " " + raw_data[2]
            cleaned_data = ""
            if "degrees C" in raw_data:
                cleaned_data = [raw_data[0], raw_data[5], value, raw_data[14], raw_data[11]]
                result.append(cleaned_data)
            if "Volts" in raw_data:
                cleaned_data = [raw_data[0], raw_data[5], value]    
                result.append(cleaned_data)
        return result

    def getNodeInfoByType(self, nodeID, sensor_type_list):
        code = ""
        message = ""
        result_list = []
        for sensor_type in sensor_type_list:
            sensor_data = []
            command = "ipmitool -I lanplus -H %s -U root -P pdclab sdr elist full | grep %s" % (self.ipmi_IP_dict[nodeID], sensor_type)
            try:
	        p = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=True)
                return_code = p.wait()
                if return_code !=0: # check subprocess's status
                    raise Exception("Error! The subprocess's command is invalid")
                    #raise subprocess.CalledProcessError(return_code, command)
	        while True:
	    	    sensor_info = p.stdout.readline()
                    if not sensor_info:
                        break
                    sensor_data.append(sensor_info)
                # data clean
                sensor_data = self.handleRawData(sensor_data, sensor_type, nodeID, self.ipmi_IP_dict[nodeID])
                result_list.append(sensor_data)
                code = "0"
                message = message + "Successfully get computing node : %s's %s information." % (nodeID, sensor_type)
                logging.info("IpmiModule getNodeInfo - " + message)
	    except Exception as e:
                message = message + "Error! Unable to get computing node : %s's %s information." % (nodeID, sensor_type)
                logging.error("IpmiModule getNodeInfo - %s" % e)
                code = "1"
                break
        
        result = {"code":code, "info":result_list, "message":message}
        return result

    def handleRawData(selr, raw_data_list, sensor_type, nodeID, nodeIP):
        result = []
        for raw_data in raw_data_list:
            cleaned_data = [] # to store the data which want to show to user
            # example of raw_data: Temp             | 0Eh | ok  |  3.1 | 45 degrees C
            # we wnat to get "Temp"
            sensor_ID = raw_data.split("|")[0].split("  ")[0]  # get sensor ID by raw_data
            cleaned_data.append(sensor_ID)
            command = 'ipmitool -I lanplus -H %s -U root -P pdclab sdr get "%s"' % (nodeIP, sensor_ID)
	    try:
	        p = subprocess.Popen(command, stdout = subprocess.PIPE , stderr = subprocess.PIPE, shell=True)
                return_code = p.wait()
                if return_code !=0: # check subprocess's status
                    raise Exception("Error! The subprocess's command is invalid")
                    #raise subprocess.CalledProcessError(return_code, command)
                while True:
                    detail_info = p.stdout.readline()
                    if not detail_info:
                        break
                    if 'Sensor Reading' in detail_info or 'Upper critical' in detail_info or 'Lower critical' in detail_info:
                        # use regular expression to find actual data
                        sensor_value = re.findall("[0-9\.]+", detail_info)[0]
                        cleaned_data.append(sensor_value)
                    if 'Entity ID' in detail_info:
                        # example data: "Entity ID             : 7.1 (System Board) "
                        # actual need: "System Board"
                        entity_id = detail_info.split("(")[-1][:-2]
                        cleaned_data.append(entity_id)
                result.append(cleaned_data)
            except Exception as e:
                logging.error("IpmiModule handleRawData - %s" % e)
        return result

    def checkOSstatus(self, nodeID):
        initial_timer = 0
        present_timer = 0
        detect_value = 0
        status = "OK"
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab mc watchdog get" % self.ipmi_IP_dict[nodeID]
	    p = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=True)
            return_code = p.wait()
            if return_code != 0:
                raise Exception("Error! The subprocess's command is invalid.")
            while True:
                detail_info = p.stdout.readline()
                if not detail_info:
                    break
                if 'Initial Countdown' in detail_info:
                    initial_timer = int(re.findall("[0-9]+", detail_info)[0]) # find value
                if 'Present Countdown' in detail_info:
                    present_timer = int(re.findall("[0-9]+", detail_info)[0]) # find value
            if (initial_timer - present_timer) > 4:
                #print initial_timer - present_timer
                status = "Error"
                return status
            else:
                return status
        except Exception as e:
            logging.error("IpmiModule detectOSstatus - %s" % e)
            status = "IPMI_disable"
            return status

    def checkSensorStatus(self, nodeID):
        status = "OK"
        failure_temp_list = []
        failure_volt_list = []
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab sdr elist full -v -c sensor reading" % self.ipmi_IP_dict[nodeID]
	    p = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=True)
            sensor_output, err = p.communicate()
            sensor_output = sensor_output.split("\n")[5:-1]
            status, failure_temp_list, failure_volt_list = self._checkValue(sensor_output)
            if nodeID == 'compute2':
                return "Error", failure_temp_list, failure_volt_list
            #return status, failure_temp_list, failure_volt_list
        except Exception as e:
            logging.error("IpmiModule checkSensorStatus - " + e)
            status = "IPMI_disable"
        return status, failure_temp_list, failure_volt_list
    
    def _checkValue(self, raw_data_list):
        status = "OK"
        failure_temp = []
        failure_volt = []
        for raw_data in raw_data_list:
            raw_data = raw_data.split(",")
            if raw_data[1]: # has sensor value 
                value = float(raw_data[1])
                upper_threshold = float(raw_data[11]) if raw_data[11] else value
                lower_threshold = float(raw_data[14]) if raw_data[14] else value
                if "degrees C" in raw_data:
                    if value > upper_threshold:
                        failure_temp.append((raw_data[0],"upper"))
                        status = "Error"
                    elif value < lower_threshold:
                        failure_temp.append((raw_data[0],"lower"))
                        status = "Error"
                if "Volts" in raw_data:
                    if value > upper_threshold:
                        failure_volt.append((raw_data[0],"upper"))
                        status = "Error"
                    elif value < lower_threshold:
                        failure_volt.append((raw_data[0],"lower"))
                        status = "Error"
        return status, failure_temp, failure_volt

    def resetWatchDog(self, nodeID):
        status = True
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab mc watchdog reset" % self.ipmi_IP_dict[nodeID]
            execute_message = subprocess.check_output(command, shell = True)
            if "countdown restarted" in execute_message:
                logging.info("IpmiModule resetWatchDog - The Computing Node %s's watchdog timer has been reset." % nodeID)
        except Exception as e:
            logging.error("IpmiModule resetWatchDog - %s" % e)
            status = False
        return status

    def checkPowerStatus(self, nodeID, execute_message=""):
        status = "OK"
        try:
            command = "ipmitool -I lanplus -H %s -U root -P pdclab power status" % self.ipmi_IP_dict[nodeID]
            execute_message = subprocess.check_output(command, shell = True)
            if "Power is on" not in execute_message:
                status = "Error"
            return status
        except Exception as e:
            logging.error("IpmiModule checkPowerStatus - The Compute Node %s's IPMI session can not be established." % nodeID )
            status = "IPMI_disable"
        return status
