import xmlrpclib
import ConfigParser
import argparse

from prettytable import PrettyTable

def generateTempTable(result):
    table = PrettyTable(["Sensor ID", "Device", "Value", "Lower Critical", "Upper Critical"])
    for sensor in result:
        table.add_row(sensor)
    return table

def generateVoltageTable(result):
    table = PrettyTable(["Sensor ID", "Device", "Value"])
    for sensor in result:
        table.add_row(sensor)
    return table

#global variable for sensor get mapping
sensor_mapping = {"Temp" : generateTempTable, "Voltage" : generateVoltageTable}

class bcolors:
    OK = '\033[92m'
    ERROR = '\033[91m'
    END = '\033[0m'
    
def main():

    config = ConfigParser.RawConfigParser()
    config.read('hass.conf')
    authUrl = "http://"+config.get("rpc", "rpc_username")+":"+config.get("rpc", "rpc_password")+"@127.0.0.1:"+config.get("rpc", "rpc_bind_port")
    server = xmlrpclib.ServerProxy(authUrl)

    parser = argparse.ArgumentParser(description='Openstack high availability software service(HASS)')
    subparsers = parser.add_subparsers(help='functions of HASS', dest='command')
    
    parser_cluster_create = subparsers.add_parser('cluster-create', help='Create a HA cluster')
    parser_cluster_create.add_argument("-n", "--name", help="HA cluster name", required=True)
    parser_cluster_create.add_argument("-c", "--nodes", help="Computing nodes you want to add to cluster. Use ',' to separate nodes name")

    parser_cluster_delete = subparsers.add_parser('cluster-delete', help='Delete a HA cluster')
    parser_cluster_delete.add_argument("-i", "--uuid", help="Cluster uuid you want to delete", required=True)
    
    parser_cluster_list = subparsers.add_parser('cluster-list', help='List all HA cluster')
    
    parser_node_add = subparsers.add_parser('node-add', help='Add computing node to HA cluster')
    parser_node_add.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    parser_node_add.add_argument("-c", "--nodes", help="Computing nodes you want to add to cluster. Use ',' to separate nodes name", required=True)
    
    parser_node_delete = subparsers.add_parser('node-delete', help='Delete computing node from HA cluster')
    parser_node_delete.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    parser_node_delete.add_argument("-c", "--node", help="A computing node you want to delete from cluster.", required=True)
    
    parser_node_list = subparsers.add_parser('node-list', help='List all computing nodes of Ha cluster')
    parser_node_list.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    
    parser_node_start = subparsers.add_parser('node-start', help='Power on the computing node')
    parser_node_start.add_argument("-c", "--node", help="Computing nodes you want to power on.", required=True)

    parser_node_shutOff = subparsers.add_parser('node-shutOff', help='Shut off the computing node')
    parser_node_shutOff.add_argument("-c", "--node", help="Computing nodes you want to Shut off.", required=True)

    parser_node_reboot = subparsers.add_parser('node-reboot', help='Reboot the computing node')
    parser_node_reboot.add_argument("-c", "--node", help="Computing nodes you want to reboot.", required=True)
    
    parser_node_getAllInfo = subparsers.add_parser('node-info-show', help='Get all hardware information of the computing node')
    parser_node_getAllInfo.add_argument("-c", "--node", help="Computing nodes you want to get all hardware information.", required=True)

    parser_node_getInfo_by_type = subparsers.add_parser('node-info-get', help='Get detail hardware information of the computing node')
    parser_node_getInfo_by_type.add_argument("-c", "--node", help="Computing nodes you want to get detail hardware information.", required=True)
    parser_node_getInfo_by_type.add_argument("-t", "--types", help="The type of sensors which you want to get. Use ',' to separate sensors' types", required=True)

    parser_instance_add = subparsers.add_parser('instance-add', help='Protect instance and add instance into HA cluster')
    parser_instance_add.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    parser_instance_add.add_argument("-v", "--vmid", help="The ID of the instance you wand to protect", required=True)
    
    parser_instance_delete = subparsers.add_parser('instance-delete', help='remove instance protection')
    parser_instance_delete.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    parser_instance_delete.add_argument("-v", "--vmid", help="The ID of the instance you wand to remove protection", required=True)
    
    parser_instance_list = subparsers.add_parser('instance-list', help='List all instances of Ha cluster')
    parser_instance_list.add_argument("-i", "--uuid", help="HA cluster uuid", required=True)
    
    args = parser.parse_args()
    
    if args.command == "cluster-create" :
        if args.nodes != None:
            result = server.createCluster(args.name, args.nodes.strip().split(",")).split(";")
        else:
            result = server.createCluster(args.name, []).split(";")
        print showResult(result)
    
    elif args.command == "cluster-delete" :
        result = server.deleteCluster(args.uuid).split(";")
        print showResult(result)
        
    elif args.command == "cluster-list" :
        result = server.listCluster()
        table = PrettyTable(['UUID', 'Name'])
        for (uuid, name) in result :
            table.add_row([uuid, name])
        print table
        
    elif args.command == "node-add" :
        result = server.addNode(args.uuid, args.nodes.strip().split(",")).split(";")
        print showResult(result)
        
    elif args.command == "node-delete" :
        result = server.deleteNode(args.uuid, args.node).split(";")
        print showResult(result)
        
    elif args.command == "node-list" :
        result = server.listNode(args.uuid)
        if result.split(";")[0] == '0' :
            print "Cluster uuid : " + args.uuid
            table = PrettyTable(["Count","Nodes of HA Cluster"])
            counter = 0
            for node in result.split(";")[1].split(",") :
                counter = counter + 1
                if node != '':
                    table.add_row([str(counter),node])
            print table
        else :
            print result
            
    elif args.command == "node-start" :
        result = server.startNode(args.node).split(";")
        print showResult(result)

    elif args.command == "node-shutOff" :
        result = server.shutOffNode(args.node).split(";")
        print showResult(result)

    elif args.command == "node-reboot" :
        result = server.rebootNode(args.node).split(";")
        print showResult(result)

    elif args.command == "node-info-show":
        code, result = server.getAllInfoOfNode(args.node)
        temp_list = []
        volt_list = []
        if code == '0':
            print 'Computing Node : ' + args.node
            for sensor_value in result:
                if "Temp" in sensor_value[0]:
                    temp_list.append(sensor_value)
                else:
                    volt_list.append(sensor_value)
            print "Sensor type : Temperature"
            print generateTempTable(temp_list)
            print "Sensor type : Voltage"
            print generateVoltageTable(volt_list)
        else:
            print result

    elif args.command == "node-info-get" :
        type_list = args.types.strip().split(",")
        code, result = server.getNodeInfoByType(args.node, type_list)
        if code == '0':
            print "Computing Node : " + args.node
            for sensor_type, sensor_value_list in zip(type_list, result):
                print "Sensor type : " ,sensor_type
                # get corresponding table by sensor
                table = sensor_mapping[sensor_type](sensor_value_list)
                print table
        else:
            print result

    elif args.command == "instance-add" :
        result = server.addInstance(args.uuid, args.vmid).split(";")
        print showResult(result)
        
    elif args.command == "instance-delete" :
        result = server.deleteInstance(args.uuid, args.vmid).split(";")
        print showResult(result)
        
    elif args.command == "instance-list" :
        result = server.listInstance(args.uuid)
        if result.split(";")[0] == '0' :
            print "Cluster uuid : " + args.uuid
            table = PrettyTable(["Count","Below Host", "Instance ID"])
            counter = 0
            for vmInfo in result.split(";")[1].split(",") :
                counter = counter + 1
                if vmInfo != '':
                    vm = vmInfo.split(":")
                    table.add_row([str(counter), vm[0], vm[1]])
            print table
        else :
            print result
            
def showResult(result):
    if result[0] == '0' :
        return bcolors.OK + "[Success] " + bcolors.END + result[1]
    else :
        return bcolors.ERROR + "[Error] " + bcolors.END +result[1]
    
if __name__ == "__main__":
    main()
