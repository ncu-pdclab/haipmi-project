#!/usr/bin/python
"""
Create/Delete/List cluster, node and instance.
Using openstack API to communicate with openstack
"""
from keystoneauth1.identity import v3
from keystoneauth1 import session
from novaclient import client
import logging
import ConfigParser
import time
import subprocess
import paramiko
import socket


from Cluster import Cluster   # cluster data structure class
from AccessDB import AccessDB
from IpmiModule import IPMIManager
ipmi_manager = IPMIManager()

class Recovery (object):

    def __init__ (self, system_test = False,  test=False):
        self.clusterList = {}
        self.config = ConfigParser.RawConfigParser()
        self.config.read('hass.conf')
        self.test = test   # Unit tester pass test=true. 
	self.auth = v3.Password(auth_url = 'http://controller:5000/v3',
                    	username = self.config.get("openstack", "openstack_admin_account"),
                    	password = self.config.get("openstack", "openstack_admin_password"),
                    	project_name = self.config.get("openstack", "openstack_admin_account"),
                    	user_domain_id = self.config.get("openstack", "openstack_user_domain_id"),
                    	project_domain_id = self.config.get("openstack", "openstack_project_domain_id"))
	self.port = self.config.get("detection", "polling_port")
        self.sess = session.Session(auth = self.auth)
        self.novaClient = client.Client(2.29, session = self.sess)
        self.haNode = []  # Record which node is already added to cluster.
        #if accessDB or create table failed. system will force exit.
        self.db = AccessDB()
        self.system_test = system_test
        
        if self.test == False:
            self.db.createTable()   
            self.db.readDB(self)
        
    def createCluster(self, clusterName):
        import uuid
        newClusterUuid = str(uuid.uuid4())
        newCluster = Cluster(uuid = newClusterUuid, name = clusterName)
        self.clusterList[newClusterUuid] = newCluster
        result = {"code": "0", "clusterId":newClusterUuid, "message":""}
        
        if self.test==False:
        #Unit test should not access database
            try:
                db_uuid = result["clusterId"].replace("-","")
                data = {"cluster_uuid":db_uuid, "cluster_name":clusterName}
                self.db.writeDB("ha_cluster", data)
            except:
                logging.error("Recovery Recovery - Access database failed.")
                result = {"code": "1", "clusterId":newClusterUuid, "message":"Access database failed, please wait a minute and try again."}
        return result
            
    def deleteCluster(self, uuid):
        code = ""
        message = ""
        try:
            for node in self.clusterList[uuid].nodeList:
                self.haNode.remove(node)
            del self.clusterList[uuid]
            logging.info("Recovery Recovery - The cluster %s is deleted." % uuid)
            code = "0"
            message = "The cluster %s is deleted." % uuid
            if self.test==False:
            #Unit test should not access database
                try:
                    db_uuid = uuid.replace("-","")
                    self.db.deleteData("DELETE FROM ha_cluster WHERE cluster_uuid = %s", (db_uuid,))
                except:
                    logging.error("Recovery Recovery - Access database failed.")
                    code = "1"
                    message = "Access database failed."
        except:
            logging.info("Recovery Recovery - The cluster is not found (uuid = %s)." % uuid)
            code = "1"
            message = "The cluster is not found (uuid = %s)." % uuid
            
        finally:
            result = {"code": code, "clusterId":uuid, "message":message}
            return result
        
    def listCluster(self):
        result = []
        for uuid, cluster in self.clusterList.iteritems() :
            result.append((uuid, cluster.name))
        return result
        
    def addNode(self, clusterId, nodeList, write_db = True, hostList = []):
        code = ""
        message = ""
        #query openstack node list
        if self.test == False:
            hypervisorList = self.novaClient.hypervisors.list() 
            for hypervisor in hypervisorList:
                hostList.append(str(hypervisor.hypervisor_hostname))
        else:
            write_db = False
        notMatchNode = [nodeName for nodeName in nodeList if nodeName not in hostList]
        if not notMatchNode:
            try:
                duplication_node = []
                correct_node = []
                for nodeName in nodeList:
                    if nodeName in self.haNode:
                        duplication_node.append(nodeName)
                    else:
                        correct_node.append(nodeName)

                self.clusterList[clusterId].addNode(correct_node, test = self.test)
                self.haNode.extend(correct_node)

                if duplication_node == []:
                    logging.info("Recovery Recovery - The node %s is added to cluster." % ', '.join(str(node) for node in nodeList))
                    code = "0"
                    message = "The node %s is added to cluster." % ', '.join(str(node) for node in nodeList)
                else:
                    logging.info("Recovery Recovery - overlapping node %s does not add to cluster" % ', '.join(str(node) for node in duplication_node))
                    code = "1"
                    message = "overlapping node %s does not add to cluster" % ', '.join(str(node) for node in duplication_node)
                if write_db == True:
                    try:
                        db_uuid = clusterId.replace("-", "")
                        for node in correct_node:
                            data = {"node_name": node,"below_cluster":db_uuid}
                            self.db.writeDB("ha_node", data)
                    except:
                        logging.error("Recovery Recovery - Access database failed.")
                        code = "1"
                        message = "Access database failed."
            except:
                logging.info("Recovery Recovery - The cluster is not found (uuid = %s)." % clusterId)
                code = "1"
                message = "The cluster is not found (uuid = %s)." % clusterId
                
            finally:
                result = {"code":code, "clusterId":clusterId, "message":message}
                return result
                
        else:
            logging.info("Recovery Recovery - The node is not found (name = %s)." % ', '.join(str(node) for node in notMatchNode))
            message = "The node is not found (name = %s)." % ', '.join(str(node) for node in notMatchNode)
            code = "1"
            result = {"code":code, "clusterId":clusterId, "message":message}
            return result
            
    def deleteNode(self, clusterId, nodeName):
        code = ""
        message = ""
        try:
            if nodeName not in self.clusterList[clusterId].nodeList:
                logging.info("Recovery Recovery - Delete node from cluster failed. The node is not found. (uuid = %s)" % nodeName)
                code = "1"
                message = "Delete node from cluster failed. The node is not found. (uuid = %s)" % nodeName
            else :
                self.clusterList[clusterId].deleteNode(nodeName, test = self.test)
                self.haNode.remove(nodeName)
                logging.info("Recovery Recovery - The node %s is deleted from cluster." % nodeName)
                code = "0"
                message = "The node %s is deleted from cluster." % nodeName
                if self.test == False:
                    try:
                        db_uuid = clusterId.replace("-", "")
                        self.db.deleteData("DELETE FROM ha_node WHERE node_name = %s && below_cluster = %s", (nodeName, db_uuid))
                    except:
                        logging.error("Recovery Recovery - Access database failed.")
                        code = "1"
                        message = "Access database failed."
            

        except:
            logging.info("Recovery Recovery - Delete node from cluster failed. The cluster is not found. (uuid = %s)" % clusterId)
            code = "1"
            message = "Delete node from cluster failed. The cluster is not found. (uuid = %s)" % clusterId
        finally:
            result = {"code": code, "clusterId":clusterId, "message":message}
            return result

    def listNode(self, clusterId):
        code = ""
        message = ""
        try:
            nodeList = self.clusterList[clusterId].getNode()
            code = "0"
            message = "Success"
        except:
            nodeList = ""
            code = "1"
            message = "The cluster is not found. (uuid = %s)" % clusterId
        finally:
            result = {"code": code, "nodeList":nodeList, "message":message}
            return result
            
    #def startNode(self, nodeID):
    #    code = ""
    #    message = ""
    #    try:
    #        nodeList = self.clusterList[clusterId].getNode()
    #        code = "0"
    #        message = "Success"
    #    except:
    #        nodeList = ""
    #        code = "1"
    #        message = "The cluster is not found. (uuid = %s)" % clusterId
    #    finally:
    #        result = {"code": code, "nodeList":nodeList, "message":message}
    #        return result

    def addInstance(self, clusterId, instanceId):
        code = ""
        message = ""
        try:
            test_id = self.clusterList[clusterId].instanceList
        except:
            logging.info("Recovery Recovery - Add the instance to cluster failed. The cluster is not found. (uuid = %s)" % clusterId)
            code = "1"
            message = "Add the instance to cluster failed. The cluster is not found. (uuid = %s)" % clusterId
            result = {"code": code, "clusterId":clusterId, "message":message}
            return result

        vm = self.novaClient.servers.get(instanceId)
        power_state = getattr(vm,"OS-EXT-STS:power_state")
        #power_states = [
        #'NOSTATE',      # 0x00
        #'Running',      # 0x01
        #'',             # 0x02
        #'Paused',       # 0x03
        #'Shutdown',     # 0x04
        #'',             # 0x05
        #'Crashed',      # 0x06
        #'Suspended'     # 0x07
        if power_state != 1:
            logging.info("Recovery Recovery - The instance %s can not be protected. (Not Running)" % instanceId)
            code = "1"
            message = "The instance %s can not be protected. (Not Running)" % instanceId
            
        elif [instance for instance in self.clusterList[clusterId].instanceList if instance[0]==instanceId] != [] :
            logging.info("Recovery Recovery - The instance %s is already protected." % instanceId)
            code = "1"
            message = "The instance %s is already protected." % instanceId
            
        elif self.test == False and self.novaClient.volumes.get_server_volumes(instanceId) == []:
            logging.info("Recovery Recovery - The instance %s can not be protected. (No volume)" % instanceId)
            code = "1"
            message = "The instance %s can not be protected. (No volume)" % instanceId
        else:
            if self.test == False:
                host = getattr(vm,"OS-EXT-SRV-ATTR:host")
                if host in self.clusterList[clusterId].nodeList:
                    self.clusterList[clusterId].addInstance(instanceId, host)
                    code = "0"
                    message = "The instance %s is protected." % instanceId
                else:
                    from Schedule import Schedule
                    try:
                        scheduler = Schedule()
                        target_host = scheduler.default(self.clusterList[clusterId].nodeList)
                        vm.live_migrate(host = target_host)
                        self.clusterList[clusterId].addInstance(instanceId, target_host)
                        #self._update_Instance(clusterId)
                        code = "0"
                        message = "The instance %s is migrated to host:%s and protected." % (instanceId, target_host)
                    except:
                        code = "1"
                        message = "The instance %s can not be protected. (Migrate to HA cluster failed)" % instanceId
            else:
                self.clusterList[clusterId].addInstance(instanceId, "testHost")
                code = "0"
                message = "The instance %s is protected." % instanceId
        result = {"code": code, "clusterId":clusterId, "message":message}
        return result
        
    def deleteInstance(self, clusterId, instanceId):
        code = ""
        message = ""
        try:
            instances = [instance for instance in self.clusterList[clusterId].instanceList if instance[0]==instanceId]
        except:
            logging.info("Recovery Recovery - Delete node from cluster failed. The cluster is not found. (uuid = %s)" % clusterId)
            code = "1"
            message = "Delete node from cluster failed. The cluster is not found. (uuid = %s)" % clusterId
            result = {"code": code, "clusterId":clusterId, "message":message}
            return result
        
        if instances==[] :
            logging.info("Recovery Recovery - The instance %s is not found." % instanceId)
            code = "1"
            message = "The instance %s is not found." % instanceId
        else :
            self.clusterList[clusterId].deleteInstance(instances[0])
            logging.info("Recovery Recovery - The instance %s is not protected." % instanceId)
            code = "0"
            message = "The instance %s is deleted from cluster." % instanceId
        
        result = {"code": code, "clusterId":clusterId, "message":message}
        return result
        
    def listInstance(self, clusterId):
        code = ""
        message = ""
        try:
            instanceList = self.clusterList[clusterId].getInstance()
            code = "0"
            message = "Success"
        except:
            code = "1"
            instanceList = ""
            message = "The cluster is not found. (uuid = %s)" % clusterId
        finally:
            result = {"code": code, "instanceList":instanceList, "message":message}
            return result
            
    def recoveryVM(self, clusterId, nodeName):
        print clusterId
        print nodeName
        if len(self.clusterList[clusterId].nodeList)<2:
            logging.error("Recovery Recovery - evacuate failed (cluster only 1 node)")
            return
        for instance in self.clusterList[clusterId].instanceList:
            instanceId, belowNode = instance
            if belowNode == nodeName:
                try:
                    print "Start to evacuate virtual machine from node %s" % nodeName
                    self._evacuate(instanceId, self.clusterList[clusterId].nodeList, nodeName)
                    logging.info("Recovery Recovery - The instance %s evacuate success" % instanceId)
                    print "Evacuate Finish!"
                except Exception as e:
                    print e
                    logging.error("Recovery Recovery - The instance %s evacuate failed" % instanceId)
        self.novaClient.services.force_down(nodeName, "nova-compute", False)

    def remove_node_from_cluster(self, clusterId, nodeName):
        db_uuid = clusterId.replace("-", "")
        self.haNode.remove(nodeName)
        self.db.deleteData("DELETE FROM ha_node WHERE node_name = %s && below_cluster = %s", (nodeName, db_uuid))

    def recovery_by_shut_Off_Node(self, clusterId, nodeName):
        vm_start_recovery = time.time()
        self.recoveryVM(clusterId, nodeName)
        print "And then Shut down node : %s due to sensor value critical" % nodeName 
        result = ipmi_manager.shutOffNode(nodeName)
        vm_success_evacuate = self._check_vm_status(nodeName, clusterId)
        print "VM recovery time : ", time.time() - vm_start_recovery 
        # update info after vm evacuate
        self._update_Instance(clusterId)
        message = "Recovery recovery_by_shut_Off_Node - "
        if result["code"] == 0:
            logging.info(message + result["message"])
            return True
        else:
            logging.error(message + result["message"])
            return False

    def recovery_service_failure(self, clusterId, nodeName, service_string, default_wait_time = 300):
        client = self._create_ssh_client(nodeName)
        service_mapping = {"libvirt" : "libvirt-bin", "nova" : "nova-compute", "qemukvm" : "qemu-kvm"}
        status = True

        if client:
            if "agents" in service_string:
                print "Start service failure recovery by starting Detection Agent"
                command = "cd /root/computing_node/ ; python DetectionAgent.py"
                stdin, stdout, stderr = client.exec_command(command)
                time.sleep(5)
                print "Started Detection Agent."
                client.close()
                return status
            if self.system_test:
                status = False
            else:
                service_list = service_string.split(":")[-1].split(";")[:-1]
                for service in service_list:
                    actual_service = service_mapping[service] # get actual name of service
                    command = "service " + actual_service + " restart"
                    stdin, stdout, stderr = client.exec_command(command)
                    time.sleep(10)
                
                    if stderr.readline(): # start service failure
                        logging.error("Recovery recovery_service_failure - The node %s service %s can not restart" % (nodeName, actual_service))
                        status = False
                        break

                    command = "service " + actual_service + " status"
                    stdin, stdout, stderr = client.exec_command(command + ' | grep "active" ')
                    if stdout.readline():
                        logging.info("Recovery recovery_service_failure - The node %s service %s successfully restart" % (nodeName, actual_service))
                    else:
                        logging.error("Recovery recovery_service_failure - The node %s service %s still doesn't work" % (nodeName, actual_service))
                        status = False
                        break
            if status:
                return status
            else:
                # start to evacuate vm
                vm_start_recovery = time.time()
                self.recoveryVM(clusterId, nodeName)
                print "And then reboot node : %s " % nodeName 
                host_start_recovery = time.time()
                result = ipmi_manager.rebootNode(nodeName)
                message = "Recovery recovery_service_failure - "
                if result["code"] == "0":
                    logging.info(message + result["message"])
                    vm_success_evacuate = self._check_vm_status(nodeName, clusterId)
                    print "VM recovery time : ", time.time() - vm_start_recovery + 30
                    # update info after vm evacuate
                    self._update_Instance(clusterId)
                    
                    boot_up = self._check_node_boot_success(nodeName, default_wait_time) 
                    if boot_up: #after successfully reboot, start detection agent
                        print "Node %s recovery finished!" % nodeName
                        host_recovery_time = time.time() - host_start_recovery
                        print "Host recovery time =  ", host_recovery_time
                        return True
                    else:
                        logging.error(message + "Can not reboot node %s successfully", nodeName)
                        return False
                else:
                    logging.error(message + result["message"])
                    return False
        else:
            logging.error("Recovery recovery_service_failure - The node %s can not establish ssh connection")
            return False
        
    def recovery_ipmi_daemon_failure(self, nodeName):
        client = self._create_ssh_client(nodeName)
        if client:
            stdin, stdout, stderr = client.exec_command('service openipmi restart')
            time.sleep(3)
            if stderr.readline():
                logging.error("Recovery recovery_ipmi_daemon_failure - The node %s ipmi daemon can not restart" % nodeName)
                client.close()
                return False
            stdin, stdout, stderr = client.exec_command('service openipmi status | grep "active (running)" ')
            if stdout.readline():
                logging.info("Recovery recovery_ipmi_daemon_failure - The node %s ipmi daemon successfully restart")
                client.close()
                return True
        else:
            logging.error("Recovery recovery_ipmi_daemon_failure - The node %s can not establish ssh connection")
            return False

    def recovery_watchdog_daemon_failure(self, nodeName):
        client = self._create_ssh_client(nodeName)
        if client:
            stdin, stdout, stderr = client.exec_command('service watchdog restart')
            time.sleep(3)
            if stderr.readline():
                logging.error("Recovery recovery_watchdog_daemon_failure - The node %s watchdog daemon can not restart" % nodeName)
                client.close()
                return False
            stdin, stdout, stderr = client.exec_command('service watchdog status | grep "active (running)" ')
            if stdout.readline():
                logging.info("Recovery recovery_watchdog_daemon_failure - The node %s watchdog daemon successfully restart")
                client.close()
                return True
        else:
            logging.error("Recovery recovery_watchdog_daemon_failure - The node %s can not establish ssh connection")
            return False

    def recovery_os_hanged(self, clusterId, nodeName, default_wait_time = 300):
        vm_start_recovery = time.time()
        self.recoveryVM(clusterId, nodeName)
        print "And then reboot node : %s due to os hanged" % nodeName 
        host_start_recovery = time.time()
        result = ipmi_manager.rebootNode(nodeName)
        message = "Recovery recovery_os_hanged - "
        if result["code"] == "0":
            logging.info(message + result["message"])
            vm_success_evacuate = self._check_vm_status(nodeName, clusterId)
            print "VM recovery time : ", time.time() - vm_start_recovery + 30
            # update info after vm evacuate
            self._update_Instance(clusterId)
            boot_up = self._check_node_boot_success(nodeName, default_wait_time) 
            if boot_up: #after successfully reboot, start detection agent
                print "Node %s recovery finished." % nodeName
                host_recovery_time = time.time() - host_start_recovery
                print host_recovery_time
                return True
            else:
                logging.error(message + "Can not reboot node %s successfully", nodeName)
                return False
                
        else: # can't reboot node
            logging.error(message + result["message"])
            return False

    def recovery_network_failure(self, clusterId, nodeName, default_wait_time = 300):
        #time.sleep(30)
        second_chance_status = True
        # wait 30 seconds and ping host again (second chance)
        try:
            response = subprocess.check_output(['timeout', '0.2', '-c', '1', nodeName], stderr=subprocess.STDOUT, universal_newlines=True)
        except subprocess.CalledProcessError:
            print "After 30 seconds, the network status of %s is still unrechable" % nodeName
            logging.error("Recovery recovery_network_failure - After 30 seconds, the network status of %s is still unreachable" % nodeName)
            second_chance_status = False
    
        if second_chance_status:
            print "The network status of %s returns to normal" % nodeName
            logging.info("Recovery recovery_network_failure - The network status of %s returns to normal" % nodeName)
            return True
        else:
            vm_start_recovery = time.time()
            self.recoveryVM(clusterId, nodeName)
            print "And then reboot node : %s due to network isolation" % nodeName 
            host_start_recovery = time.time()
            result = ipmi_manager.rebootNode(nodeName)
            message = "Recovery recovery_network_failure - "
            if result["code"] == "0":
                logging.info(message + result["message"])
                vm_success_evacuate = self._check_vm_status(nodeName, clusterId)
                print "VM recovery time : ", time.time() - vm_start_recovery + 30
                # update info after vm evacuate
                self._update_Instance(clusterId)
                boot_up = self._check_node_boot_success(nodeName, default_wait_time) 
                if boot_up: #after successfully reboot, start detection agent
                    print "Node %s recovery finished!" % nodeName
                    host_recovery_time = time.time() - host_start_recovery
                    print "Host recovery time : ", host_recovery_time + 30
                    return True
                else:
                    logging.error(message + "Can not reboot node %s successfully", nodeName)
                    return False
            else:
                logging.error(message + result["message"])
                return False

    def recovery_power_off(self, clusterId, nodeName, default_wait_time = 300):
        vm_start_recovery = time.time()
        self.recoveryVM(clusterId, nodeName)
        print "And then start node : %s due to accidently power off" % nodeName 
        host_start_recovery = time.time()
        result = ipmi_manager.startNode(nodeName)
        message = "Recovery recovery_power_off - "
        if result["code"] == "0":
            logging.info(message + result["message"])
            vm_success_evacuate = self._check_vm_status(nodeName, clusterId)
            print "VM recovery time : ", time.time() - vm_start_recovery + 30
            # update info after vm evacuate
            self._update_Instance(clusterId)
            boot_up = self._check_node_boot_success(nodeName, default_wait_time) 
            if boot_up: #after successfully start node, then start detection agent
                print "Node %s recoverey finished!" % nodeName
                host_recovery_time = time.time() - host_start_recovery
                print host_recovery_time
                return True
            else:
                logging.error(message + "Can not start node %s successfully", nodeName)
                return False
        else:
            logging.error(message + result["message"])
            return False
	
    def _update_Instance(self, clusterId):
        for instance in self.clusterList[clusterId].instanceList:
            vm = self.novaClient.servers.get(instance[0])
            host = getattr(vm,"OS-EXT-SRV-ATTR:host")
            instance[1] = host
            
    def _create_ssh_client(self, nodeName, default_timeout = 1):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            client.connect(nodeName, username='root', timeout = default_timeout)
            return client
        except Exception as e:
            return None

    def _check_node_boot_success(self, nodeName, check_timeout, timeout = 1):
        status = False
        data = ""
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setblocking(0)
        sock.settimeout(1)
        while "OK" not in data and check_timeout > 0:
            try:
                sock.sendto("polling request", (nodeName, int(self.port)))
                data, addr = sock.recvfrom(2048)
                if "OK" in data:
                    status = True
            except Exception as e:
                print e
            finally:
                time.sleep(1)
                check_timeout -= 1
        return status

    def _check_vm_status(self, nodeName, clusterId, check_timeout = 60):
        status = False
        for instance in self.clusterList[clusterId].instanceList:
            instanceId, belowNode = instance
            if belowNode == nodeName:
                instance = self.novaClient.servers.get(instanceId)
                ip = str(instance.networks['provider'][0])
                check_timeout = 60
                while (not status) and check_timeout > 0:
                    try:
                        response = subprocess.check_output(['timeout', '0.2', 'ping', '-c', '1' , ip], stderr=subprocess.STDOUT, universal_newlines=True)
                        status = True
                    except subprocess.CalledProcessError:
                        status = False
                    finally:
                        time.sleep(0.2)
                        check_timeout -= 0.2
        return status
        
    def _evacuate(self, instanceId, nodeList, failednode):
        from Schedule import Schedule
        import subprocess
        schedule = Schedule()
        instance = self.novaClient.servers.get(instanceId)
        target_host = schedule.default(nodeList, failednode)
        try:
            self.novaClient.services.force_down(failednode, "nova-compute", True)
	    self.novaClient.servers.evacuate(instance, target_host)
        except:
            raise
